
import 'package:flutter/material.dart';

class Problem3 extends StatefulWidget {
  const Problem3({super.key});

  @override
  State createState() => _Problem3State();
}

class _Problem3State extends State {
  bool isColorChange = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              (isColorChange) ? isColorChange = false : isColorChange = true;
            });
          },
          child: Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              border: Border.all(
                color: (isColorChange) ? Colors.red : Colors.green,
                width: 4,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
