import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.blue,
                border: Border.all(
                  width: 10,
                  color: Colors.red,
                )),
            height: 300,
            width: 300,
          ),
        ),
      ),
    );
  }
}
