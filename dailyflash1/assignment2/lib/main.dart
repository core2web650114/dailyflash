import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          leading: const Icon(Icons.home),
          actions: const [
            Icon(Icons.settings),
            Icon(Icons.search),
            Icon(Icons.more_vert)
          ],
          centerTitle: true,
          title: const Text("AppBar"),
        ),
      ),
    );
  }
}
