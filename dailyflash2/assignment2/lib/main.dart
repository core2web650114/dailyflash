import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
              padding: const EdgeInsets.all(30),
              decoration: const BoxDecoration(
                  color: Colors.blue,
                  border: Border(
                    left: BorderSide(width: 5, color: Colors.black),
                  )),
              height: 100,
              width: 100,
              child: const Text("hello")),
        ),
      ),
    );
  }
}
