import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 217, 144, 230),
                borderRadius:
                    const BorderRadius.only(topRight: Radius.circular(18)),
                border: Border.all(
                  width: 4,
                  color: const Color.fromARGB(255, 173, 37, 197),
                )),
            height: 100,
            width: 100,
          ),
        ),
      ),
    );
  }
}
